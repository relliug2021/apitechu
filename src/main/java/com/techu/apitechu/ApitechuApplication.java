package com.techu.apitechu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApitechuApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);
	}

}
